package com.david.crawler.constants;

/**
 * 常量url的提取
 * 
 * @author ：David
 * @weibo ：http://weibo.com/mcxiaobing
 * @github: https://github.com/QQ986945193
 */
public class Constants {

	public static final String PIAOHUA_VIDEO_URL = "https://www.piaohua.com/html/dongzuo/list_";

	public static final String ZH = "https://amr.sz.gov.cn/0501W/Iframe/LicItemIframe.aspx?licId=7c2a767b-1426-43bd-8c7e-a314cd76684a&page=";
}
