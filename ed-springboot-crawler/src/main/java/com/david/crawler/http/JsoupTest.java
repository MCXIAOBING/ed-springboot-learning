package com.david.crawler.http;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * @author David Zheng
 * @date 2024/09/18
 * @email dmcxiaobing@foxmail.com
 */
public class JsoupTest {
    public static void main(String[] args) {
        // 示例HTML字符串
        String html = "<html><head><title>Sample Title</title></head>"
                + "<body><p id='test'>Sample Content</p></body></html>";

        // 使用Jsoup解析HTML
        Document doc = Jsoup.parse(html);

        // 根据ID获取元素
        Element element = doc.getElementById("test");

        // 输出元素的内容
        System.out.println(element.text());
    }
}
