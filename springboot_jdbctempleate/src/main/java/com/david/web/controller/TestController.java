package com.david.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhengchuanwei
 *
 *
 * executeXxx() : 执行任何SQL语句，对数据库、表进行新建、修改、删除操作
 * updateXxx() : 执行新增、修改、删除等语句
 * queryXxx() : 执行查询相关的语句
 */
@RequestMapping("/test")
@RestController
public class TestController {

	@Resource
	private JdbcTemplate jdbcTemplate;

	@Resource
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


	@GetMapping("/jdbcQ")
	public String jdbcQ() throws IOException {


		List<Map<String, Object>> mapList = jdbcTemplate.queryForList("select * from t_device limit 20");
		for(Map<String, Object> device : mapList){
			System.out.println("id = " + device.get("id") + ", sn = " + device.get("sn") + ", mac = " + device.get("mac"));
		}
		return "";
	}


	@GetMapping("/jdbcUpdate")
	public String jdbcUpdate( HttpServletRequest request,
							 HttpServletResponse response) throws IOException {
		//插入数据
		jdbcTemplate.update("insert into tb_user(name, age) values (?, ?)", "张三", 20);

		//插入返回主键
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String addSql = "insert into tb_user(name, age) values (?, ?)";
				PreparedStatement ps = connection.prepareStatement(addSql,PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, "李四");
				ps.setInt(2, 43);
				return ps;
			}
		}, keyHolder);
		int keyId = keyHolder.getKey().intValue();
		System.out.println("插入的数据Id = " + keyId);




//		//插入数据
//		MapSqlParameterSource param = new MapSqlParameterSource();
//		param.addValue("name", "王五");
//		param.addValue("age", 25);
//		namedParameterJdbcTemplate.update("insert into tb_user(name, age) values (:name, :age)", param);
//
//		//插入返回主键
//		KeyHolder keyHolder = new GeneratedKeyHolder();
//		MapSqlParameterSource param1 = new MapSqlParameterSource();
//		param1.addValue("name", "李白");
//		param1.addValue("age", 28);
//		namedParameterJdbcTemplate.update("insert into tb_user(name, age) values (:name, :age)", param1, keyHolder);
//		int keyId = keyHolder.getKey().intValue();
//		System.out.println("插入的数据Id = " + keyId);
		return "";
	}
}
